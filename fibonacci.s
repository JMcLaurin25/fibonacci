.intel_syntax noprefix

.data
.fib_num1:
    .quad   0x1
    .quad   0x0
    .quad   0x0
    .quad   0x0
.fib_num2:
    .quad   0x0
    .quad   0x0
    .quad   0x0
    .quad   0x0
.fib_res:
    .quad   0x0
    .quad   0x0
    .quad   0x0
    .quad   0x0

value:
    .long   0


.text
Request:
    .asciz  "Please enter the desired Fibonacci Number (0-200): "

Fail:
    .asciz  "Invalid value.\n"

Fail_OF:
    .asciz  "Number too large\n"

str_fmt:
    .asciz  "%s"

Result:
    .asciz  "0x%llx%llx%llx%llx\n"

# Function get_val() derived from .c get value function, using scanf
.LC0:
        .string "%d"
get_val:
    push    rbp
    mov	    rbp, rsp
    sub	    rsp, 16
    mov	    QWORD PTR [rbp-8], rdi
    mov	    rax, QWORD PTR [rbp-8]
    mov	    rsi, rax
    mov	    edi, OFFSET FLAT:.LC0
    xor	    eax, eax
    call    scanf
    leave
    ret

# Referenced https://en.wikibooks.org/wiki/Python_Programming/Loops
# Fibonacci function to have 1 argument. The int nth fibonacci number.
# http://x86asm.net/articles/working-with-big-numbers-using-x86-instructions/
Fibonacci:	# Using for-loop
    #add	    QWORD [.fib_num1], 0x1
    mov	    rcx, rdi
    test    rcx, rcx
    jz	    2f
1:
    xor	    rax, rax    

    # step 1: Sum the two values into fib_num1
    mov	    rax, [.fib_num2]
    add	    [.fib_num1], rax
    mov	    rax, [.fib_num2 + 8]
    adc	    [.fib_num1 + 8], rax
    mov	    rax, [.fib_num2 + 16]
    adc	    [.fib_num1 + 16], rax
    mov	    rax, [.fib_num2 + 24]
    adc	    [.fib_num1 + 24], rax

    # step 2: store result from fib_num1
    mov	    rax, [.fib_num1]
    mov	    [.fib_res], rax
    mov	    rax, [.fib_num1 + 8]
    mov	    [.fib_res + 8], rax
    mov	    rax, [.fib_num1 +16]
    mov	    [.fib_res + 16], rax
    mov	    rax, [.fib_num1 + 24]
    mov	    [.fib_res + 24], rax

    # step 3: store new fib_num1, from fib_num2
    mov	    rax, [.fib_num2]
    mov	    [.fib_num1], rax
    mov	    rax, [.fib_num2 + 8]
    mov	    [.fib_num1 + 8], rax
    mov	    rax, [.fib_num2 + 16]
    mov	    [.fib_num1 + 16], rax
    mov	    rax, [.fib_num2 + 24]
    mov	    [.fib_num1 + 24], rax

    # step 4: store new fib_num2, from fib_res
    mov	    rax, [.fib_res]
    mov	    [.fib_num2], rax
    mov	    rax, [.fib_res + 8]
    mov	    [.fib_num2 + 8], rax
    mov	    rax, [.fib_res + 16]
    mov	    [.fib_num2 + 16], rax
    mov	    rax, [.fib_res + 24]
    mov	    [.fib_num2 + 24], rax

    sub	    rcx, 0x1
    jnz	    1b

2:	    # The compared value is zero
    xor	    rax, rax
    ret

.globl main
main:
    xor	    rax, rax

    mov	    rsi, OFFSET Request
    mov	    rdi, OFFSET str_fmt
    call    printf

    push    rbp

    xor	    rdi, rdi
    lea	    rbx, [value]
    mov	    rdi, rbx
    call    get_val

    cmp	    eax, 0
    je	    1f

    # Set flags to test value input
    cmp    DWORD PTR [value], 0
    jl	    1f

    # Compare if value too large 370 is max
    cmp	    DWORD PTR [value], 370
    jg	    overflow

    # Run through Fibonacci function
    mov	    edi, DWORD PTR[value]
    call    Fibonacci

    # Results output
    mov	    rdi, OFFSET Result
    mov	    rsi, [.fib_res + 24]
    mov	    rdx, [.fib_res + 16]
    mov	    rcx, [.fib_res + 8]
    mov	    r8, [.fib_res]

    call    printf

    jmp	    2f

overflow: # Jump here if overflow occurs
    mov	    rdi, OFFSET Fail_OF
    mov	    rsi, OFFSET str_fmt
    call    printf
    jmp	    2f

1:	# Jump here if Invalid Value
    mov	    rdi, OFFSET Fail
    mov	    rsi, OFFSET str_fmt
    call    printf

2:
    pop	    rbp    
    ret

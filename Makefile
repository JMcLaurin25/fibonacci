CFLAGS += -O1 -masm=intel -fno-asynchronous-unwind-tables
ASFLAGS += -W

%.s: %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -S -o $@ $^

fibonacci: fibonacci.s

.PHONY: clean

clean:
	-rm fibonacci

# Fibonacci

The Fibonacci Sequence is a famous mathematical sequence that is defined as follows:

F0 = 0
F1 = 1
FN = F(n-1) + F(n-2)

The first few terms of the Fibonacci sequence are 0, 1, 1, 2, 3, 5, 8, 13,...

Write a program in Assembly Language that prompts the user to enter a number N from 0 through 200.
The program should then print out F(N), in hexadecimal.

# Sample Output
> fibonacci
Please enter the desired Fibonacci Number (0-200): 47
0xB11924E1

> fibonacci
Please enter the desired Fibonacci Number (0-200): 29
0x7D8B5

# Requirements

The program should be in a git repository called fibonacci. Running make from the top-level directory should produce a binary program of the same name.

It is completely acceptable to start from a C version, and compile it to assembly to determine how to proceed. The final version that builds, however, must only use assembly language.

# Flourishes

Support up to the 300th Fibonacci number.

Challenge: Print the value in decimal, rather than hex.

Advice

Any function calls (such as to printf or scanf) will not override the Base, Base Pointer, or r11-r15 registers. These can be used to store data between function calls. Alternatively, static storage may be used.
